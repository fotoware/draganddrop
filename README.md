# Welcome to the FotoWeb Drag and Drop app

This app is a sample to show how you can implement drag and drop from FotoWeb to an external app (this app is the external app). 

The main files that you need to read to understand how it works is /Controllers/HomeController.cs and /wwwroot/js/site.js . The rest is just scaffolding for a .net app.

Please note that this app is hardcoded to use our demo-server (http://demo.fotoware.com)

Enjoy!