
function getAssets(data) {
$.ajax({
  method: "POST",
  url: 'home/GetAssets',
  data: {url: data}
})
  .done(function( msg ) {
    msg.forEach(function(element) {
        createImageElement(element);
    }, this);
  });
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    var text = document.getElementById("dropText");
    if (text)
        text.remove();
    getAssets(data);
}

function createImageElement(data) {
    
    var container = document.getElementById('div1');

    var imageDiv = document.createElement('div');
    imageDiv.className = "imageContainer";

    var textDiv = document.createElement('div');
    textDiv.className = "textDiv";
    
    var fileName = document.createElement('div');
    fileName.className = "fileName";
    fileName.innerHTML = "<b>Filename: </b>" + data.fileName;

    var title = document.createElement('div');
    title.className = "title";
    title.innerHTML = "<b>Title: </b>" + data.title;

    var description = document.createElement('div');
    description.className = "description";
    description.innerHTML = "<b>Description: </b>" + data.description;

    

    var img = document.createElement('img');

    // Hard coded host name. Need to be changed for other sites
    img.src = 'http://demo.fotoware.com' + data.href;

    // Add each node parsed from the response from the HomeController
    textDiv.appendChild(fileName);
    textDiv.appendChild(title);
    textDiv.appendChild(description);
    imageDiv.appendChild(img);
    imageDiv.appendChild(textDiv);
    container.appendChild(imageDiv);
}