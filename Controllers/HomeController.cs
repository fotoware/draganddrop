using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using Newtonsoft.Json;

namespace DragAndDrop.Controllers
{
    public class HomeController : Controller
    {

        public List<Asset> assetList;
        
        public IActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public async Task<List<Asset>> GetAssets() {
            var url = Request.Form["url"];            
            await setAssetList(url);
            return assetList;
        }

        public async Task setAssetList(string url)
        {
            // Here you set the site the app should work towards
            string serverUrl = "http://demo.fotoware.com";

            // Very important the encode the url received from the drop event
            string encodedUrl = System.Net.WebUtility.UrlEncode(url);
            assetList = new List<Asset>();
            

            using (var client = new HttpClient()) {
                try {
                    client.BaseAddress = new Uri(serverUrl);
                    //Set accept header
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/vnd.fotoware.assetlist+json");
                    
                    // Send request to the url resolver service in FotoWeb
                    var response = await client.GetAsync("/fotoweb/services/urlresolver?url=" +   encodedUrl);
                    response.EnsureSuccessStatusCode();

                    var stringResponse = await response.Content.ReadAsStringAsync();
                    var obj = JsonConvert.DeserializeObject<dynamic>(stringResponse);

                    // Parse the response from FotoWeb
                    foreach(var asset in obj["data"])
                    {
                        string previewHref = asset["previews"][5]["href"];
                        string filename = asset["filename"];
                        string description = asset["metadata"]["120"] != null ? asset["metadata"]["120"]["value"] : "";
                        string title = asset["metadata"]["5"] != null ? asset["metadata"]["5"]["value"] : "";

                        Asset item = new Asset{
                            href = previewHref, 
                            fileName = filename,
                            description = description,
                            title = title
                        };
                        assetList.Add(item);
                        
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine($"Request exception: {e.Message}");
                }
            }
        }
    }

    public class Asset
    {
        public string href { get; set; }
        public string fileName { get; set; }
        public string description { get; set; }
        public string title { get; set; }
    }
}
